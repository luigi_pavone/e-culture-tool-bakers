package com.example.e_culturetoolbakers;

import androidx.annotation.Keep;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Toast;

import com.example.e_culturetoolbakers.database.DatabaseHelper;
import com.example.e_culturetoolbakers.model.Museo;
import com.example.e_culturetoolbakers.model.Zona;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

public class testFirebase extends AppCompatActivity {

    /// ACTIVITY DA USARE SOLO PER TEST

    private StorageReference mStorageRef;
    private FirebaseDatabase database;
    private DatabaseHelper db_helper;
    private ArrayList<Museo> listaMusei;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_firebase);

        db_helper = new DatabaseHelper(this);
        //db_helper.inserisciTest();
        listaMusei = db_helper.getListMuseo();


        mStorageRef = FirebaseStorage.getInstance().getReference();
        database = FirebaseDatabase.getInstance("https://e-culture-tool-c381c-default-rtdb.europe-west1.firebasedatabase.app/");
        DatabaseReference ref = database.getReference("Musei");

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    System.out.println(dataSnapshot.getValue());
                    String nome = dataSnapshot.child("nome").getValue().toString();
                    String info = dataSnapshot.child("info").getValue().toString();
                    String tipo = dataSnapshot.child("tipo").getValue().toString();

                    Museo museo = new Museo(nome, info, tipo);

                    boolean trov = false;
                    for(Museo museoTemp : listaMusei){
                        if(museoTemp.getNome().equals(museo.getNome())){
                            //Trovato museo
                            trov = true;

                            db_helper.updateMuseo(nome, info, tipo);

                        }
                    }

                    if(trov == false){
                        db_helper.inserisciMuseo(nome, info, tipo);
                        int id = db_helper.getIdMuseo(nome);

                        // Ciclo tra le zone
                        for(DataSnapshot snapshot1 : dataSnapshot.child("zone").getChildren()) {

                            String nomeZona = snapshot1.getKey().toString();
                            String descrizioneZona = snapshot1.child("descrizione").getValue().toString();
                            String fotoZona = snapshot1.child("foto").getValue().toString();

                            Zona zona = new Zona(nomeZona, descrizioneZona, fotoZona);

                            db_helper.inserisciZona(nomeZona, descrizioneZona, id, fotoZona);
                            int id_zona = db_helper.getIdZona(nomeZona);

                            // Aggiunge qui la zona altrimenti, uscendo dal secondo foreach, si perde il ref alle opere.
                            museo.addZona(zona.getNome(), zona.getDescrizione(), zona.getFoto());

                            // Ciclo tra le opere
                            for (DataSnapshot snapshot2 : snapshot1.child("opere").getChildren()) {

                                String nomeOpera = snapshot2.getKey().toString();
                                String descrizioneOpera = snapshot2.child("descrizione").getValue().toString();
                                String fotoOpera = snapshot2.child("foto").getValue().toString();

                                db_helper.inserisciOpera(nomeOpera, descrizioneOpera, id_zona, fotoOpera);

                                museo.findZona(nomeZona).addOpera(nomeOpera, descrizioneOpera, fotoOpera);
                            }
                        }
                    }


                    // Dopo questo blocco si perde ref all'oggetto museo.
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });



    }
}